//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by Роман Хоменко on 08.02.2022.
//

import Foundation
import SwiftUI


class EmojiMemoryGame: ObservableObject {
    typealias Card = MemoryGame<String>.Card
    
    static let emojis = "🏎🚒🚜🚂🚗🚕🛻🚐🌵☘️🌿🎍🍀🌸🍂🌹🌼🌻" // for plug
    
    private static func createMemoryGame() -> MemoryGame<String> {
        MemoryGame<String>(numberOfPairsOfCards: emojis.count) { pairIndex in
            String(EmojiMemoryGame.emojis[pairIndex])
        }
    }
    
    @Published private(set) var model: MemoryGame<String>! = nil
//    @Published private var theme: Theme
    
    init() {
//        theme = Theme(named: "Cars", emojis: ["🏎", "🚒", "🚜", "🚂", "🚗", "🚕", "🛻", "🚖"], colourOfCards: "yellow", id: 0)
        
        model = EmojiMemoryGame.createMemoryGame()
    }
    
    
    var cards: Array<Card> {
        model.cards
    }
    
    // MARK: - Intent(s)
    
//    var themeSet: Theme {
//        theme
//    }
    
    func scoreCount() -> Int {
        model.scoreCounter
    }
    
    func choose(_ card: Card) {
        model.choose(card)
    }
    
//    func newGame() {
//        theme.arrayOfThemes = []
//        theme.arrayOfThemes.append(Theme(name: "Cars", emojis: ["🏎", "🚒", "🚜", "🚂", "🚗", "🚕", "🛻", "🚐"].shuffled(), colourOfCards: "red"))
//        theme.arrayOfThemes.append(Theme(name: "Flowers", emojis: ["🌵", "☘️", "🌿", "🎍", "🍀", "🌸", "🍂", "🌹", "🌼", "🌻"].shuffled(), colourOfCards: "yellow"))
//        theme.arrayOfThemes.append(Theme(name: "Random", emojis: ["🏎", "🚒", "🚜", "🚂", "🌿", "🎍", "🍀", "🌸", "🚕", "🛻", "🚖", "🌻"].shuffled(), colourOfCards: "blue"))
//
//        theme = theme.arrayOfThemes[Int.random(in: 0..<theme.arrayOfThemes.count)]
//        model = EmojiMemoryGame.createMemoryGame(with: theme)
//    }
}
