//
//  EmojiMemoryGameView.swift
//  Memorize
//
//  Created by Роман Хоменко on 04.02.2022.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    @ObservedObject var game: EmojiMemoryGame
    
    var body: some View {
        VStack {
            HStack {
                label
                Spacer()
                HStack {
                    scoreLabel
                    scoreCounter
                }
            }
            AspectVGrid(items: game.cards, aspectRatio: 2/3, content: { card in
                if card.isMatched && !card.isFaceUp {
                    Rectangle().opacity(0)
                } else {
                    CardView(card: card)
                        .padding(4)
                        .onTapGesture {
                            game.choose(card)
                        }
                }
            })
//            .foregroundColor(colorOfCards(currentColor: game.themeSet.colourOfCards))
                .foregroundColor(.red)
            Spacer()
            HStack {
//                newGame
            }
            .font(.largeTitle)
            .padding(.horizontal)
        }
        .padding(.horizontal)
    }
    
    var label: some View {
//        Text(game.themeSet.name)
        Text("Some game")
            .font(.largeTitle)
    }
    
    var scoreLabel: some View {
        Text("Score: ").font(.largeTitle)
    }
    
    var scoreCounter: some View {
        Text("\(game.scoreCount())").font(.largeTitle)
    }
    
//    var newGame: some View {
//        VStack {
//            Button {
//                game.newGame()
//            } label: {
//                VStack {
//                    Text("New Game").font(.largeTitle)
//                }
//            }
//        }
//    }
}


struct CardView: View {
    let card: EmojiMemoryGame.Card
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Pie(startAngle: Angle(degrees: 0-90), endAngle: Angle(degrees: 120-90))
                    .padding(4).opacity(0.5)
                Text(card.content)
                    .rotationEffect(Angle.degrees(card.isMatched ? 360 : 0))
                    .animation(Animation.linear(duration: 2).repeatForever(autoreverses: false))
                    .font(Font.system(size: DrawingConstants.fontSize))
                    .scaleEffect(scale(thatFits: geometry.size))
            }
            .cardify(isFaceUp: card.isFaceUp)
        }
    }
    
    private func scale(thatFits size: CGSize) -> CGFloat {
        min(size.width, size.height) / (DrawingConstants.fontSize / DrawingConstants.fontScale)
    }
    
    private struct DrawingConstants {
        static let fontScale: CGFloat = 0.65
        static let fontSize: CGFloat = 32
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let game = EmojiMemoryGame()
        game.choose(game.cards.first!)
//        EmojiMemoryGameView(game: game)
//            .preferredColorScheme(.dark)
        return EmojiMemoryGameView(game: game)
    }
}

// MARK: - UI - dependent func that change color of cards

func colorOfCards(currentColor: String) -> Color {
    switch currentColor {
        case "red":
            return Color.red
        case "yellow":
            return Color.yellow
        case "blue":
            return Color.blue
        default:
            return Color.white
    }
}
