//
//  MemoryGame.swift
//  Memorize
//
//  Created by Роман Хоменко on 08.02.2022.
//

import Foundation

struct MemoryGame<CardContent> where CardContent: Equatable {
    private(set) var cards: Array<Card>
    
    private var indexOfTheOneAndOnlyFaceUpCard: Int? {
        get { cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly }
        set { cards.indices.forEach { cards[$0].isFaceUp = ($0 == newValue) } }
    }
    var scoreCounter: Int = 0 // Simplify it later?
    
    mutating func choose(_ card: Card) {
        if let chosenIndex = cards.firstIndex(where: { $0.id == card.id }),
           !cards[chosenIndex].isFaceUp,
           !cards[chosenIndex].isMatched
        {
            if let potentialMachIndex = indexOfTheOneAndOnlyFaceUpCard {
                if cards[chosenIndex].content == cards[potentialMachIndex].content {
                    cards[chosenIndex].isMatched = true
                    cards[potentialMachIndex].isMatched = true
                    scoreCounter += 2
                }
                cards[chosenIndex].isFaceUp = true
                
                // score game logic
                if cards[chosenIndex].wasTouched == true && cards[potentialMachIndex].wasTouched == true && cards[chosenIndex].content != cards[potentialMachIndex].content {
                    scoreCounter -= 2
                } else if cards[chosenIndex].wasTouched == true && cards[potentialMachIndex].wasTouched == false && cards[chosenIndex].content != cards[potentialMachIndex].content {
                    cards[potentialMachIndex].wasTouched = true
                    scoreCounter -= 1
                } else if cards[chosenIndex].wasTouched == false && cards[potentialMachIndex].wasTouched == true && cards[chosenIndex].content != cards[potentialMachIndex].content {
                    cards[chosenIndex].wasTouched = true
                    scoreCounter -= 1
                } else {
                    cards[potentialMachIndex].wasTouched = true
                    cards[chosenIndex].wasTouched = true
                }
                
            } else {
                indexOfTheOneAndOnlyFaceUpCard = chosenIndex
            }
        }
    }
    
    init(numberOfPairsOfCards: Int, createCardContent: (Int) -> CardContent) {
        cards = []
        // add numbersOfPairsOfCards x 2 cards to cards array
        for pairIndex in 0..<numberOfPairsOfCards {
            let content = createCardContent(pairIndex)
            cards.append(Card(id: pairIndex*2, content: content))
            cards.append(Card(id: pairIndex*2+1, content: content))
        }
        cards.shuffle()
    }
    
    struct Card: Identifiable {
        let id: Int
        
        var isFaceUp = false
        var isMatched = false
        let content: CardContent
        var wasTouched = false
    }
}
