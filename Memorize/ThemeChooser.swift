//
//  ThemeChooser.swift
//  Memorize
//
//  Created by Роман Хоменко on 14.03.2022.
//

import SwiftUI

struct Theme: Identifiable, Codable {
    var name: String
    var emojis: String
    var colourOfCards: String
    var id: Int
    
    var pairsOfCards: Int {
        return emojis.count
    }
    
    fileprivate init(name: String, emojis: String, colourOfCards: String, id: Int) {
        self.name = name
        self.emojis = emojis
        self.colourOfCards = colourOfCards
        self.id = id
    }
}

class ThemeChooser: ObservableObject {
    let name: String
    
    @Published var themes = [Theme]() {
        didSet {
            storeInUserDefaults()
        }
    }
    
    private var userDefaultsKey: String {
        "Theme Chooser: " + name
    }
    
    private func storeInUserDefaults() {
        UserDefaults.standard.set(try? JSONEncoder().encode(themes), forKey: userDefaultsKey)
//        UserDefaults.standard.set(themes.map { [$0.name, $0.emojis, $0.colourOfCards, String($0.id)] }, forKey: userDefaultsKey)
    }
    
    private func restoreFromUserDefaults() {
        if let jsonData = UserDefaults.standard.data(forKey: userDefaultsKey), let decodedThemes = try? JSONDecoder().decode([Theme].self, from: jsonData) {
            themes = decodedThemes
        }
                
//        if let themesAsPropertyList = UserDefaults.standard.array(forKey: userDefaultsKey) as? [[String]] {
//            for themeAsArray in themesAsPropertyList {
//                if themeAsArray.count == 4, let id = Int(themeAsArray[3]), !themes.contains(where: { $0.id == id }) {
//                    let theme = Theme(name: themeAsArray[0], emojis: themeAsArray[1], colourOfCards: themeAsArray[2], id: id)
//                    themes.append(theme)
//                }
//            }
//        }
    }
    
    init(named name: String) {
        self.name = name
        restoreFromUserDefaults()
        if themes.isEmpty {
            insertTheme(named: "Cars", emojis: "🏎🚒🚜🚂🚗🚕🛻🚐", colour: "red")
            insertTheme(named: "Flowers", emojis: "🌵☘️🌿🎍🍀🌸🍂🌹🌼🌻", colour: "yellow")
            insertTheme(named: "Random", emojis: "🏎🚒🚜🚂🚗🚕🛻🚐🌵☘️🌿🎍🍀🌸🍂🌹🌼🌻", colour: "blue")
        }
    }
    
    // MARK: - Intents
    
    func theme(at index: Int) -> Theme {
        let safeIndex = min(max(index, 0), themes.count - 1)
        return themes[safeIndex]
    }
    
    @discardableResult
    func removePalette(at index: Int) -> Int {
        if themes.count > 1, themes.indices.contains(index) {
            themes.remove(at: index)
        }
        return index % themes.count
    }
    
    func insertTheme(named name: String, emojis: String? = nil, colour: String, at index: Int = 0) {
        let unique = (themes.max(by: { $0.id < $1.id })?.id ?? 0) + 1
        let theme = Theme(name: name, emojis: emojis ?? "", colourOfCards: colour, id: unique)
        let safeIndex = min(max(index, 0), themes.count)
        themes.insert(theme, at: safeIndex)
    }
}
