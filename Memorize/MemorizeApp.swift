//
//  MemorizeApp.swift
//  Memorize
//
//  Created by Роман Хоменко on 04.02.2022.
//

import SwiftUI

@main
struct MemorizeApp: App {
    private let game = EmojiMemoryGame()
    private let themeChooser = ThemeChooser(named: "Theme Chooser")
    
    var body: some Scene {
        WindowGroup {
            EmojiMemoryGameView(game: game)
        }
    }
}
